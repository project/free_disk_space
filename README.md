# Table of Contents

 - [Introduction](#introduction)
 - [Features](#features)
 - [Requirements](#requirements)
 - [Installation](#installation)
 - [Usage/Configuration](#usageconfiguration)
 - [Supporting organizations](#supporting-organizations)
 - [Maintainers](#maintainers)

## Introduction
Free Disk Space module lets you know when your system or server is running low
on disk space. Will log or add warning during cron run and also integrates with
Rules. Can execute rule action when low disk space rule condition is met.

## Features
- Can configure low disk space threshold.
- Can configure directory/path to check low disk space on.
- Adds log/warning message during cron run if low disk space  is detected.
- Low disk space Rules condition which can be used to trigger emails
    or other actions.
- Simple overview page that let's you know total & remaining disk space.
- Useful for users with limited access to server.

## Requirements
- Requires the [Rules](https://www.drupal.org/project/rules) module.
Which then requires the
[Typed Data API enhancements](https://www.drupal.org/project/typed_data) module.

## Installation
Normal module installation through Composer / Version History.
Once the module is in its proper modules directory, finish installing it by
going to the "Extend" tab in the admin pages, search for "Free Disk Space"
under the "Other" category. Be sure to install the required dependencies.

[More information](https://www.drupal.org/docs/extending-drupal/installing-modules)

## Usage/Configuration
1. Install the module.
2. Configure settings for low disk space. Found in
    ***/admin/config/free-disk-space***.
3. Setup Rules integration or simply review overview page.
4. In case of low disk space, visit the ***/admin/reports/dblog***
    report page and search for a log message of type ***free_disk_space*** for
    more information on the warning.
5. Configure the ***Administer Free Disk Space*** permission if needed.

#### Supporting organizations
- [Boxnerd Canada](https://www.drupal.org/boxnerd-canada)

#### Maintainers
- [geoanders](https://www.drupal.org/u/geoanders)

Currently considered feature complete.
