<?php

namespace Drupal\free_disk_space;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\free_disk_space\Event\LowDiskSpaceEvent;

/**
 * Free disk space service.
 */
class FreeDiskSpaceService {

  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Config factory object.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get config.
   *
   * @return \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   *   Returns editable config object.
   */
  public function getConfig() {
    return $this->configFactory->getEditable('free_disk_space.settings');
  }

  /**
   * Determine is low disk space has been reached.
   *
   * @return bool
   *   Returns true if low disk space is detected, otherwise false.
   */
  public function isLowDiskSpace(): bool {
    $config = $this->getConfig();

    if (empty($config->get('directory'))) {
      return FALSE;
    }

    if (empty($config->get('threshold'))) {
      return FALSE;
    }

    $free_space_bytes = disk_free_space($config->get('directory'));
    $threshold_bytes = $this->human2byte($config->get('threshold') . 'M');
    if ($free_space_bytes <= $threshold_bytes) {
      $this->getLogger('free_disk_space')
        ->warning($this->t('Low disk space. Total remaining space left %space', ['%space' => $this->getFreeDiskSpace()]));
      $this->triggerLowDiskSpaceEvent();
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Trigger low disk space event.
   */
  public function triggerLowDiskSpaceEvent() {
    $event = new LowDiskSpaceEvent();
    // phpcs:disable
    $event_dispatcher = \Drupal::service('event_dispatcher');
    // phpcs:enable
    $event_dispatcher->dispatch(LowDiskSpaceEvent::EVENT_NAME, $event);
  }

  /**
   * Get free disk space amount.
   *
   * @return false|string
   *   Returns free disk space left.
   */
  public function getFreeDiskSpace() {
    $config = $this->getConfig();

    if (empty($config->get('directory'))) {
      return FALSE;
    }

    return $this->formatSizeUnits(disk_free_space($config->get('directory')));
  }

  /**
   * Get total disk space amount.
   *
   * @param string $directory
   *   The directory to check disk space on.
   *
   * @return false|string
   *   Returns total disk space left at said directory.
   */
  public function getTotalDiskSpace(string $directory = '') {
    $config = $this->getConfig();

    if (empty($directory) && !empty($config->get('directory'))) {
      $directory = $config->get('directory');
    }

    if (empty($directory)) {
      return FALSE;
    }

    return $this->formatSizeUnits(disk_total_space($directory));
  }

  /**
   * Get directory size.
   *
   * @param string $directory
   *   The directory or path.
   *
   * @return false|int
   *   Returns the directory size or false otherwise.
   */
  public function getDirectorySize(string $directory) {
    $size = 0;
    $files = glob($directory . '/*');
    foreach ($files as $path) {
      is_file($path) && $size += filesize($path);
      is_dir($path) && $size += $this->getDirectorySize($path);
    }
    return $size;
  }

  /**
   * Format bytes to readable format.
   *
   * @param float $bytes
   *   The total amount of bytes.
   *
   * @return string
   *   Return readable string/format of total bytes.
   */
  public function formatSizeUnits(float $bytes): string {
    if ($bytes >= 1073741824) {
      $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576) {
      $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024) {
      $bytes = number_format($bytes / 1024, 2) . ' KB';
    }
    elseif ($bytes > 1) {
      $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1) {
      $bytes = $bytes . ' byte';
    }
    else {
      $bytes = '0 bytes';
    }

    return $bytes;
  }

  /**
   * Converts human-readable file size to a number of bytes that it represents.
   *
   * Supports the following modifiers: K, M, G and T.
   *
   * Example:
   * <code>
   * $config->human2byte(10);          // 10
   * $config->human2byte('10b');       // 10
   * $config->human2byte('10k');       // 10240
   * $config->human2byte('10K');       // 10240
   * $config->human2byte('10kb');      // 10240
   * $config->human2byte('10Kb');      // 10240
   * </code>
   *
   * @param string $value
   *   The value.
   *
   * @return array|string|string[]
   *   Returns human-readable file size.
   */
  private function human2byte(string $value) {
    return preg_replace_callback('/^\s*(\d+)\s*(?:([kmgt]?)b?)?\s*$/i', function ($m) {
      switch (strtolower($m[2])) {
        case 't':
          $m[1] *= 1024;
        case 'g':
          $m[1] *= 1024;
        case 'm':
          $m[1] *= 1024;
        case 'k':
          $m[1] *= 1024;
      }
      return $m[1];
    }, $value);
  }

}
