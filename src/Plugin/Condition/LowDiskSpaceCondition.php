<?php

namespace Drupal\free_disk_space\Plugin\Condition;

use Drupal\rules\Core\RulesConditionBase;

/**
 * Provides a 'Low disk space' condition.
 *
 * @Condition(
 *   id = "rules_low_disk_space",
 *   label = @Translation("Low disk space"),
 *   category = @Translation("System"),
 *   context_definitions = {}
 * )
 */
class LowDiskSpaceCondition extends RulesConditionBase {

  /**
   * Check to see if free disk space is low or not.
   *
   * @return bool
   *   TRUE if disk space is low.
   *   FALSE if disk space is fine.
   */
  protected function doEvaluate(): bool {
    /** @var \Drupal\free_disk_space\FreeDiskSpaceService $freeDiskSpace */
    $freeDiskSpace = \Drupal::service('free_disk_space');
    return $freeDiskSpace->isLowDiskSpace();
  }

}
