<?php

namespace Drupal\free_disk_space\Event;

use Symfony\Component\EventDispatcher\GenericEvent;

/**
 * Event that is fired when low disk space.
 */
class LowDiskSpaceEvent extends GenericEvent {

  const EVENT_NAME = 'rules_low_disk_space';

}
