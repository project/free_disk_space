<?php

namespace Drupal\free_disk_space\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\free_disk_space\FreeDiskSpaceService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Free disk space controller.
 *
 * @package Drupal\free_disk_space\Controller
 */
class FreeDiskSpaceController extends ControllerBase {

  /**
   * The free disk space service.
   *
   * @var \Drupal\free_disk_space\FreeDiskSpaceService
   */
  protected $freeDiskSpaceService;

  /**
   * Free disk space controller constructor.
   *
   * @param \Drupal\free_disk_space\FreeDiskSpaceService $freeDiskSpaceService
   *   The free disk space service.
   */
  public function __construct(FreeDiskSpaceService $freeDiskSpaceService) {
    $this->freeDiskSpaceService = $freeDiskSpaceService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('free_disk_space')
    );
  }

  /**
   * Disk space overview.
   *
   * @return array
   *   Returns an overview.
   */
  public function getOverview(): array {
    $build = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Label'),
        $this->t('Directory'),
        $this->t('Disk space'),
      ],
    ];

    $build['#rows'][] = [
      $this->t('Drupal instance'),
      DRUPAL_ROOT,
      $this->freeDiskSpaceService->formatSizeUnits($this->freeDiskSpaceService->getDirectorySize(DRUPAL_ROOT)),
    ];

    $build['#rows'][] = [
      $this->t('Free disk space'),
      $this->freeDiskSpaceService->getConfig()->get('directory'),
      $this->freeDiskSpaceService->getFreeDiskSpace(),
    ];

    $build['#rows'][] = [
      $this->t('Total disk space'),
      $this->freeDiskSpaceService->getConfig()->get('directory'),
      $this->freeDiskSpaceService->getTotalDiskSpace(),
    ];

    return $build;
  }

  /**
   * Triggers low disk space event.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns or redirects back to overview page.
   */
  public function triggerEvent(): RedirectResponse {
    $this->freeDiskSpaceService->triggerLowDiskSpaceEvent();
    $this->messenger()->addMessage($this->t('Low disk space event was triggered.'));
    return $this->redirect('free_disk_space.overview');
  }

}
