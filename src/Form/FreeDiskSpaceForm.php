<?php

namespace Drupal\free_disk_space\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Free Disk Space Form.
 *
 * @package Drupal\free_disk_space\Form
 */
class FreeDiskSpaceForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'free_disk_space_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['free_disk_space.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('free_disk_space.settings');

    $form['general'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Free Disk Space Settings'),
    ];

    $form['general']['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory/path to check'),
      '#description' => $this->t('The directory or path which will be used for checking for free disk space. Defaults to /.'),
      '#required' => TRUE,
      '#default_value' => !empty($config->get('directory')) ? $config->get('directory') : '/',
    ];

    $form['general']['threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Low disk space threshold (MB)'),
      '#description' => $this->t('Set the threshold for disk space to be considered low. Defaults to 500MB.'),
      '#required' => TRUE,
      '#min' => 0,
      '#default_value' => !empty($config->get('threshold')) ? $config->get('threshold') : 500,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('free_disk_space.settings')
      ->set('directory', $form_state->getValue('directory'))
      ->set('threshold', $form_state->getValue('threshold'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
